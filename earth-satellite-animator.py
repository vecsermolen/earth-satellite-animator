import time
import requests
from datetime import datetime, timezone, timedelta

from sgp4.earth_gravity import wgs72, wgs84
from sgp4.io import twoline2rv
from vtkplotter import *

# Whether to output a video of the simulation
OUTPUT_VIDEO = False


def main():
    # All lengths in 10^6 m

    vp = Plotter(title='The World', bg2=(0, 0, 0), bg=(40, 40, 40), interactive=False, axes=4, size=(600, 800))

    earth = vp.sphere((0, 0, 0), r=6.371, c='blue')
    vp.addActor(earth)

    gps_sats = get_gps_satellites()

    sat_models = []
    for sat in gps_sats:
        model = vp.sphere((0, 0, 0), r=0.1, c='red')
        model.addTrail(alpha=0.4, maxlength=5, n=100)
        vp.addActor(model)
        sat_models.append((sat, model))

    cam = vp.camera
    cam.Azimuth(-15)
    cam.Elevation(-5)

    pb = ProgressBar(0, 500)
    start = datetime.now(timezone.utc)
    dt = timedelta(minutes=1)

    first_run = True

    if OUTPUT_VIDEO:
        video = vp.openVideo(name='satellites.mp4', duration=30)

    for i in pb.range():
        now = sgp4_datetime(start + i * dt)

        for sat, model in sat_models:
            pos_km, vel = sat.propagate(*now)
            pos = [x * 1e-3 for x in pos_km]
            model.pos(pos)

        if first_run:
            vp.show(interactive=False, resetcam=True)
            first_run = False

        cam.Azimuth(0.15)
        cam.Elevation(0.03)

        vp.render()
        pb.print()

        if OUTPUT_VIDEO:
            video.addFrame()
        else:
            time.sleep(1/60.0)

    if OUTPUT_VIDEO:
        video.close()

    vp.show(interactive=True, resetcam=False)


def get_celestrak_tle(catnr: int):
    """ Gets a specified two-line element (TLE) representing a satellite orbit from CelesTrak

    :param catnr: the 5-digit NORAD catalog number for the satellite
    :returns: a list of three strings with the satellite data: [name, TLE1, TLE2]
    """
    r = requests.get('http://celestrak.com/satcat/tle.php', params={'CATNR': catnr})
    # Raise HTTP errors as exception
    r.raise_for_status()
    resp_lines = iter(r.text.splitlines())
    # Satellite data is the 3 lines after the <pre> tag
    for line in resp_lines:
        if line.startswith('<pre>'):
            break
    return [next(resp_lines) for i in range(3)]


def get_satellite_data(catnr: int, use_wgs84=True):
    """ Creates a SGP4 satellite model from online data, given the satellite's catalog number

    :param catnr: the 5-digit NORAD catalog number for the satellite
    :param use_wgs84: if True (default), use the WGS84 earth model for calculations, otherwise use the WGS72
    :returns: a satellite object from the Python SGP4 library
    """
    tle = get_celestrak_tle(catnr)
    model = wgs84 if use_wgs84 else wgs72
    return twoline2rv(tle[1], tle[2], model)


def get_gps_satellites():
    r = requests.get('http://celestrak.com/NORAD/elements/gps-ops.txt')
    # Raise HTTP errors as exception
    r.raise_for_status()
    resp_lines = iter(r.text.splitlines())
    sats = []
    try:
        while True:  # Broken by StopIteration
            tle = [next(resp_lines) for i in range(3)]
            sats.append(twoline2rv(tle[1], tle[2], wgs84))
    except StopIteration:
        pass
    return sats


def sgp4_datetime(dt: datetime):
    """ Converts a Python datetime object to a tuple suitable for the Python SGP4 library

    Note: Output time is converted to UTC. If dt.tzinfo is not set, it is assumed to be UTC time already.

    :param dt: a datetime object, assumed to be UTC time if tzinfo is not set
    :returns: a tuple that can be passed to directly to the SGP4 library: (year, month, day, hour, minute, second)
    """
    dt_utc = dt
    # Assume UTC time, if unspecified
    if dt.tzinfo is None:
        dt_utc = dt.replace(tzinfo=timezone.utc)
    # Convert to UTC time
    dt_utc = dt_utc.astimezone(timezone.utc)
    return (
    dt_utc.year, dt_utc.month, dt_utc.day, dt_utc.hour, dt_utc.minute, dt_utc.second + dt_utc.microsecond * 1e-6)


if __name__ == '__main__':
    main()
